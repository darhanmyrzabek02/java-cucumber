# API test weather

Feature: Test API for weather station

  Scenario Outline: Test API of cities and countries in weather api
    Given Weather station URL is running
    When  The user sents GET request with <city>
    Then Response Code 200 is validated
    Then <city> city name is correct
    Then <country> country name is correct
    Examples:
      | city     | country                  |
      | London   | United Kingdom           |
      | New York | United States of America |
      | Tokyo    | Japan                    |
      | Almaty   | Kazakhstan               |

  Scenario Outline: Test weather api timezone
    Given Weather station URL is running
    When  The user sents GET request with <city>
    Then Response Code 200 is validated
    Then <utc> utc is correct
    Then <timezone> timezone is correct
    Examples:
      | city     | utc  | timezone         |
      | London   | 1.0  | Europe/London    |
      | New York | -4.0 | America/New_York |
      | Tokyo    | 9.0  | Asia/Tokyo       |
      | Almaty   | 6.0  | Asia/Almaty      |


  Scenario: Test weather api negative case without access key
    Given Weather station URL is running
    When  The user sents GET request without access key
    Then Response Code is 101

  Scenario Outline: Test weather api negative case with incorrect data
    Given Weather station URL is running
    When The user sents GET request with <city>
    Then Response Code is 615
    Examples:
      | city        |
      | LondonTest  |
      | NewYorkTest |

  Scenario Outline: Test weather api negative case with incorrect language
    Given Weather station URL is running
    When Sent GET request with "<city>" with incorrect language
    Then Response Code is 605
    Examples:
      | city     |
      | London   |
      | New York |

  Scenario Outline: Test weather api negative case with incorrect units
    Given Weather station URL is running
    When Sent GET request with "<city>" with incorrect units
    Then Response Code is 606
    Examples:
      | city     |
      | London   |
      | New York |


