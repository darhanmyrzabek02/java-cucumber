package runner;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
        features="features",
        glue="stepDefinition",
        plugin={"json:target/cucumber.json","pretty","html:target/cucumber-reports"}
)

public class TestRunnerNew {

}

