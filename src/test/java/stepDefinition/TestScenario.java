package stepDefinition;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


import static constants.Constants.ACCESS_KEY;
import static constants.Constants.BASE_URL;
import static org.junit.Assert.assertEquals;


public class TestScenario {
    private Scenario scenario;

    private Response response;

    @Before
    public void before(Scenario scenarioVal) {
        this.scenario = scenarioVal;
    }

    @Given("^Weather station URL is running$")
    public void Weather_station_URL_is_running() throws Throwable {
        RestAssured.baseURI = BASE_URL;
    }

    @When("The user sents GET request with (.*)")
    public void get_particular_city(String city) throws Throwable {

        RequestSpecification requestSpecification = RestAssured.given()
                .queryParam("access_key", ACCESS_KEY)
                .queryParam("query", city);
        response = requestSpecification.when().get("/current");
    }

    @When("The user sents GET request without access key")
    public void get_particular_city_without_access() throws Throwable {

        RequestSpecification requestSpecification = RestAssured.given()
                .queryParam("access_key", "");
        response = requestSpecification.when().get("/current");
    }

    @When("Sent GET request with (.*) with incorrect language")
    public void get_particular_city_with_incorrect_language(String city) throws Throwable {

        RequestSpecification requestSpecification = RestAssured.given()
                .queryParam("access_key", ACCESS_KEY)
                .queryParam("query", city)
                .queryParam("language", "en");
        response = requestSpecification.when().get("/current");
    }

    @When("Sent GET request with (.*) with incorrect units")
    public void get_particular_city_with_incorrect_units(String city) throws Throwable {

        RequestSpecification requestSpecification = RestAssured.given()
                .queryParam("access_key", ACCESS_KEY)
                .queryParam("query", city)
                .queryParam("units", "1000");
        response = requestSpecification.when().get("/current");
    }

    @Then("(.*) city name is correct")
    public void assert_city_name_response(String cityName) {
        JsonPath jsonpath = response.jsonPath();
        String name = jsonpath.getString("location.name");
        assertEquals(cityName, name);
    }

    @Then("(.*) country name is correct")
    public void assert_country_name_response(String countryName) {
        JsonPath jsonpath = response.jsonPath();
        String name = jsonpath.getString("location.country");
        assertEquals(countryName, name);
    }

    @Then("^(.*) utc is correct$")
    public void assert_utc_response(String utcName) {
        JsonPath jsonpath = response.jsonPath();
        String name = jsonpath.getString("location.utc_offset");
        assertEquals(utcName, name);
    }

    @Then("^(.*) timezone is correct$")
    public void assert_timezone_name_response(String timezoneName) {
        JsonPath jsonpath = response.jsonPath();
        String name = jsonpath.getString("location.timezone_id");
        assertEquals(timezoneName, name);
    }

    @Then("Response Code 200 is validated")
    public void response_is_validated() {
        int responseCode = response.then().extract().statusCode();
        assertEquals("200", String.valueOf(responseCode));
    }


    @Then("Response Code is 615")
    public void response_is_failed() {
        JsonPath jsonpath = response.jsonPath();
        String statusCode = jsonpath.getString("error.code");
        assertEquals("615", String.valueOf(statusCode));
    }

    @Then("Response Code is 101")
    public void not_authorized() {
        JsonPath jsonpath = response.jsonPath();
        String statusCode = jsonpath.getString("error.code");
        assertEquals("101", String.valueOf(statusCode));
    }

    @Then("Response Code is 605")
    public void incorrect_language() {
        JsonPath jsonpath = response.jsonPath();
        String statusCode = jsonpath.getString("error.code");
        assertEquals("605", String.valueOf(statusCode));
    }

    @Then("Response Code is 606")
    public void invalid_unit() {
        JsonPath jsonpath = response.jsonPath();
        String statusCode = jsonpath.getString("error.code");
        assertEquals("606", String.valueOf(statusCode));
    }
}
